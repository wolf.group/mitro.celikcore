# Mitro Celik Core

Ovo je projekat nastao na osnovu inicijalne CelikNET implementacije: https://www.elitesecurity.org//t415641-CelikNET-LGPL-CLR-Interop-Wrapper-za-CelikAPI


TODO: Unaprediti readme file

Q&A:

1. Kako se koristi?
A: Dodati CelikNET u reference, u deployment dodati i CelikAPI jer ga CelikNET referencira.

2. Zasto CelikAPI nije u binaries?
A: Nisam siguran koja mu je licenca pa ga nisam ukljucio, u svakom slucaju imate radnu verziju u source arhivi

3. Zasto LGPL?
A: Cilj mi nije bio da zaradim pare na ovome, proizvod je besplatan samo ne zelim da ga neko prodaje za svoj ceh. LGPL umesto GPL da biste mogli da ga koristite u sopstvenim komercijalnim aplikacijama bez da ste nesto duzni meni ili komuni. Ako me se setite u About boxu za dan Bezbednosti, setite, ako ne to je bila moja gradjanska duznost. 

4. User manual?
A: Mnogo hocete  Pogledajte celikAPI doc, od mene ovoliko:

``` csharp
using (CelikNET.CelikInterop x = new CelikNET.CelikInterop())
{
    CelikNET.FixedPersonalData d1 = x.ReadFixedPersonalData();
    CelikNET.VariablePersonalData d2 = x.ReadVariablePersonalData();
    CelikNET.DocumentData d3 = x.ReadDocumentData();
    Bitmap c = x.ReadProfileImage();
}
 ```

5. Sta je sa inicijalizacijom/cleanup-om?
A: Static type konstruktor inicijalizuje API, application cleanup ga cisti (preko destruktora static singletona). Sam CelikInterop koristi Disposable pattern za Begin/End read, tako da je sve transparentno, koristite using(...) {} i uzivajte

6. WPF ne koristi Bitmap image, kako da prikazem sliku
A: Postoji konvertor koji prebacuje GDI Bitmap u WPF BitmapSource, proguglajte

7. Podrska za cirilicu?
A: Tu je, cirilicna polja se vracaju kao unicode cirilica

8. Sta je CelikException?
A: Njega baca CelikNET kad neka od CelikAPI funkcija vrati negativan rezultat. CelikException Ima sledeca vazna polja (primer)
EIDErrorCode -> -1
EIDErrorDescriptionEn -> "General error"
EIDErrorDescriptionSr -> "Opšta greška" // <- cirilicom

9. Multithreading?
A: Sve instance metode su thread safe (serijalizuje se pristup APIu). Medjutim u ovom trenutku isntancijacija nije serijalizovana, ukoliko se uspostavi potreba za ozbiljan multithreading support, dodacemo.

