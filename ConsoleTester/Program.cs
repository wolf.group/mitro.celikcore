﻿using System;
using System.Linq;
using System.Text;
using WSCT.Wrapper.Desktop.Core;

// ReSharper disable StringLiteralTypo

namespace ConsoleTester
{
    internal class Program
    {
        private static int Main()
        {
            // Connect to PC / SC
            var context = new CardContext();
            context.Establish();

            // Get installed readers
            context.ListReaders("");
            var allReaders = context.Readers;
            context.Release();

            if (!allReaders.Any()) 
            {
                Console.WriteLine("Nije detektovan nijedan SmartCard čitač!");
                return -1;
            }

            Console.WriteLine("Detektovani su sledeći SmartCard čitači:");
            foreach (var cr in allReaders) Console.WriteLine(cr);
            Console.WriteLine($"Koristimo prvi detektovan SmartCard čitač sa spiska: {allReaders[0]}");

            Console.OutputEncoding = Encoding.GetEncoding(1251);
            try
            {
                var cardReader = $"{allReaders[0]}\0";
                var cardReaderBytes = Encoding.ASCII.GetBytes(cardReader);


                unsafe
                {
                    sbyte* sp;

                    fixed (byte* p = cardReaderBytes)
                    {
                        sp = (sbyte*)p;
                        //SP is now what you want
                    }

                    using (var x = new CelikNET.CelikInterop(sp))
                    {
                        Console.WriteLine("--- Podaci o ličnoj karti ---------------------------------------");
                        var d3 = x.ReadDocumentData();
                        Console.WriteLine($"Registarski broj (docRegNo)          : {d3.docRegNo}");
                        Console.WriteLine($"Datum izdavanja  (issuingDate)       : {d3.issuingDate}");
                        Console.WriteLine($"Važi do          (expiryDate)        : {d3.expiryDate}");
                        Console.WriteLine($"Dokument izdaje  (issuingAuthority)  : {d3.issuingAuthority}");

                        Console.WriteLine("--- Fiksni podaci -----------------------------------------------");
                        var d1 = x.ReadFixedPersonalData();
                        Console.WriteLine($"JMBG            (personalNumber)     : {d1.personalNumber}");
                        Console.WriteLine($"Prezime         (surname)            : {d1.surname}");
                        Console.WriteLine($"Ime             (givenName)          : {d1.givenName}");
                        Console.WriteLine($"Pol             (sex)                : {d1.sex}");
                        Console.WriteLine($"Mesto rođenja   (placeOfBirth)       : {d1.placeOfBirth}");
                        Console.WriteLine($"Država rođenja  (stateOfBirth)       : {d1.stateOfBirth}");
                        Console.WriteLine($"Datum rođenja   (dateOfBirth)        : {d1.dateOfBirth}");
                        Console.WriteLine($"Opština rođenja (communityOfBirth)   : {d1.communityOfBirth}");

                        Console.WriteLine("--- Varijabilni podaci ------------------------------------------");
                        var d2 = x.ReadVariablePersonalData();
                        Console.WriteLine($"Država stanovanja  (state)           : {d2.state}");
                        Console.WriteLine($"Opština stanovanja (community)       : {d2.community}");
                        Console.WriteLine($"Mesto stanovanja   (place)           : {d2.place}");
                        Console.WriteLine($"Ulica              (street)          : {d2.street}");
                        Console.WriteLine($"Kućni broj         (houseNumber)     : {d2.houseNumber}");
                        Console.WriteLine($"Kućno slovo        (houseLetter)     : {d2.houseLetter}");
                        Console.WriteLine($"Ulaz               (entrance)        : {d2.entrance}");
                        Console.WriteLine($"Sprat              (floor)           : {d2.floor}");
                        Console.WriteLine($"Broj apartmana     (apartmentNumber) : {d2.apartmentNumber}");

                        Console.WriteLine("--- Slika -------------------------------------------------------");
                        var c = x.ReadProfileImage();
                        Console.WriteLine($"Učitana slika veličine {c.Width} x {c.Height} piksela");
                    }
                    Console.ReadLine();
                }
            }
            catch (CelikNET.CelikException ex)
            {
                Console.WriteLine("EXCEPTION: {0} {1}", ex.EIDErrorCode, ex.EIDErrorDescriptionEn);
                Console.ReadLine();
                return -1;
            }

            return 0;
        }

        //public static byte[] GetBytesFromStringWithZero(Encoding encoding, string str)
        //{
        //    var len = encoding.GetByteCount(str);

        //    // Here we leave a "space" for the ending \0
        //    // Note the trick to discover the length of the \0 in the encoding:
        //    // It could be 1 (for Ansi, Utf8, ...), 2 (for Unicode, UnicodeBE), 4 (for UTF32)
        //    // We simply ask the encoder how long it would be to encode it :-)
        //    var bytes = new byte[len + encoding.GetByteCount("\0")];
        //    encoding.GetBytes(str, 0, str.Length, bytes, 0);
        //    return bytes;
        //}
    }
}
